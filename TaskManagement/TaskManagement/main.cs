﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaskManagement.Properties;
using System.Timers;
namespace TaskManagement
{
    public partial class main : Form
    {
        private BAACDataContext database;
        private TaskManagement taskManagement;
        private System.Timers.Timer aTimer;
        public main()
        {
            InitializeComponent();
            database = new BAACDataContext(Settings.Default.conectionString);
            taskManagement = new TaskManagement();
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            DataGridViewRow r = DGV_TASKS.CurrentRow;
            int id = int.Parse(DGV_TASKS.Rows[r.Index].Cells[0].Value.ToString());
            TASK task = database.TASKs.Where(x => x.ID == id).FirstOrDefault();
            taskManagement.AddTask(task);
        }

        private void main_Shown(object sender, EventArgs e)
        {
            DGV_TASKS.DataSource = database.TASKs.Where(x => x.STATUS == "N" || x.STATUS == "S").ToList();

            aTimer = new System.Timers.Timer(5);
            aTimer.Elapsed += OnTimedEvent;
        }
        private void OnTimedEvent(Object source, ElapsedEventArgs e) {

        }
        private void main_Load(object sender, EventArgs e)
        {
            
        }

        private void main_FormClosing(object sender, FormClosingEventArgs e)
        {
            database.Dispose();
        }

        private void DGV_TASKS_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            loadJob();
        }
        private void loadJob()
        {
            DataGridViewRow r = DGV_TASKS.CurrentRow;
            int id = int.Parse(DGV_TASKS.Rows[r.Index].Cells[0].Value.ToString());
            TASK task = database.TASKs.Where(x => x.ID == id).FirstOrDefault();
            DGV_JOBS.DataSource = task.JOBs;

        }
        private void btn_refresh_Click(object sender, EventArgs e)
        {
            DGV_TASKS.DataSource = database.TASKs.ToList();
        }

        private void btn_job_refresh_Click(object sender, EventArgs e)
        {
            loadJob();
        }

        private void btnStartService_Click(object sender, EventArgs e)
        {
            lbServiceStatus.Text = "Start";
        }

        private void btnStopService_Click(object sender, EventArgs e)
        {
            lbServiceStatus.Text = "Stop";
        }
    }
}
