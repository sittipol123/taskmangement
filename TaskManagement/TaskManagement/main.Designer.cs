﻿namespace TaskManagement
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DGV_TASKS = new System.Windows.Forms.DataGridView();
            this.DGV_JOBS = new System.Windows.Forms.DataGridView();
            this.btn_start = new System.Windows.Forms.Button();
            this.btn_doAjob = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_stop = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_refresh = new System.Windows.Forms.Button();
            this.btn_job_refresh = new System.Windows.Forms.Button();
            this.btnStartService = new System.Windows.Forms.Button();
            this.btnStopService = new System.Windows.Forms.Button();
            this.lbServiceStatus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_TASKS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_JOBS)).BeginInit();
            this.SuspendLayout();
            // 
            // DGV_TASKS
            // 
            this.DGV_TASKS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_TASKS.Location = new System.Drawing.Point(12, 62);
            this.DGV_TASKS.MultiSelect = false;
            this.DGV_TASKS.Name = "DGV_TASKS";
            this.DGV_TASKS.ReadOnly = true;
            this.DGV_TASKS.RowTemplate.Height = 24;
            this.DGV_TASKS.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV_TASKS.Size = new System.Drawing.Size(558, 253);
            this.DGV_TASKS.TabIndex = 0;
            this.DGV_TASKS.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DGV_TASKS_CellMouseClick);
            // 
            // DGV_JOBS
            // 
            this.DGV_JOBS.AllowUserToAddRows = false;
            this.DGV_JOBS.AllowUserToDeleteRows = false;
            this.DGV_JOBS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_JOBS.Location = new System.Drawing.Point(589, 62);
            this.DGV_JOBS.MultiSelect = false;
            this.DGV_JOBS.Name = "DGV_JOBS";
            this.DGV_JOBS.ReadOnly = true;
            this.DGV_JOBS.RowTemplate.Height = 24;
            this.DGV_JOBS.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGV_JOBS.Size = new System.Drawing.Size(558, 253);
            this.DGV_JOBS.TabIndex = 1;
            // 
            // btn_start
            // 
            this.btn_start.Location = new System.Drawing.Point(12, 337);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(93, 46);
            this.btn_start.TabIndex = 2;
            this.btn_start.Text = "Start task";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // btn_doAjob
            // 
            this.btn_doAjob.Location = new System.Drawing.Point(589, 337);
            this.btn_doAjob.Name = "btn_doAjob";
            this.btn_doAjob.Size = new System.Drawing.Size(93, 46);
            this.btn_doAjob.TabIndex = 3;
            this.btn_doAjob.Text = "Run a job";
            this.btn_doAjob.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(713, 337);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(107, 46);
            this.button1.TabIndex = 4;
            this.button1.Text = "Stop a job";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // btn_stop
            // 
            this.btn_stop.Location = new System.Drawing.Point(122, 337);
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(93, 46);
            this.btn_stop.TabIndex = 5;
            this.btn_stop.Text = "Stop task";
            this.btn_stop.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Tasks";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(586, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Jobs";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(110, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(312, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Status: P = processing, N = new, E = end, F=Fail";
            // 
            // btn_refresh
            // 
            this.btn_refresh.Location = new System.Drawing.Point(231, 337);
            this.btn_refresh.Name = "btn_refresh";
            this.btn_refresh.Size = new System.Drawing.Size(94, 46);
            this.btn_refresh.TabIndex = 9;
            this.btn_refresh.Text = "Refresh";
            this.btn_refresh.UseVisualStyleBackColor = true;
            this.btn_refresh.Click += new System.EventHandler(this.btn_refresh_Click);
            // 
            // btn_job_refresh
            // 
            this.btn_job_refresh.Location = new System.Drawing.Point(837, 337);
            this.btn_job_refresh.Name = "btn_job_refresh";
            this.btn_job_refresh.Size = new System.Drawing.Size(94, 46);
            this.btn_job_refresh.TabIndex = 10;
            this.btn_job_refresh.Text = "Refresh";
            this.btn_job_refresh.UseVisualStyleBackColor = true;
            this.btn_job_refresh.Click += new System.EventHandler(this.btn_job_refresh_Click);
            // 
            // btnStartService
            // 
            this.btnStartService.Location = new System.Drawing.Point(111, 405);
            this.btnStartService.Name = "btnStartService";
            this.btnStartService.Size = new System.Drawing.Size(104, 46);
            this.btnStartService.TabIndex = 11;
            this.btnStartService.Text = "Start service";
            this.btnStartService.UseVisualStyleBackColor = true;
            this.btnStartService.Click += new System.EventHandler(this.btnStartService_Click);
            // 
            // btnStopService
            // 
            this.btnStopService.Location = new System.Drawing.Point(221, 405);
            this.btnStopService.Name = "btnStopService";
            this.btnStopService.Size = new System.Drawing.Size(104, 46);
            this.btnStopService.TabIndex = 12;
            this.btnStopService.Text = "Stop service";
            this.btnStopService.UseVisualStyleBackColor = true;
            this.btnStopService.Click += new System.EventHandler(this.btnStopService_Click);
            // 
            // lbServiceStatus
            // 
            this.lbServiceStatus.AutoSize = true;
            this.lbServiceStatus.Location = new System.Drawing.Point(39, 420);
            this.lbServiceStatus.Name = "lbServiceStatus";
            this.lbServiceStatus.Size = new System.Drawing.Size(37, 17);
            this.lbServiceStatus.TabIndex = 13;
            this.lbServiceStatus.Text = "Stop";
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1166, 515);
            this.Controls.Add(this.lbServiceStatus);
            this.Controls.Add(this.btnStopService);
            this.Controls.Add(this.btnStartService);
            this.Controls.Add(this.btn_job_refresh);
            this.Controls.Add(this.btn_refresh);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_stop);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_doAjob);
            this.Controls.Add(this.btn_start);
            this.Controls.Add(this.DGV_JOBS);
            this.Controls.Add(this.DGV_TASKS);
            this.Name = "main";
            this.Text = "Task management";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.main_FormClosing);
            this.Load += new System.EventHandler(this.main_Load);
            this.Shown += new System.EventHandler(this.main_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_TASKS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_JOBS)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DGV_TASKS;
        private System.Windows.Forms.DataGridView DGV_JOBS;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Button btn_doAjob;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btn_stop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_refresh;
        private System.Windows.Forms.Button btn_job_refresh;
        private System.Windows.Forms.Button btnStartService;
        private System.Windows.Forms.Button btnStopService;
        private System.Windows.Forms.Label lbServiceStatus;
    }
}

