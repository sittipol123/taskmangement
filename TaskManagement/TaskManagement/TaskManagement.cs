﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaskManagement.Properties;
namespace TaskManagement
{
    class Process {
        private BAACDataContext databaseTask;
        public Process() {

        }

             
        
    }

    class TaskManagement
    {
        
        List<Task> tasks = new List<Task>();
        //databaseTask = new BAACDataContext(Settings.Default.conectionString);
        public TaskManagement()
        {
           
        }
        public async void AddTask(TASK task) {
           
            foreach (JOB job in task.JOBs)
            {
                Task runTask = Run(job.ID);               
                await runTask;
            }
           
        }

        public bool DirectoryExists(string directory, NetworkCredential networkCredential)
        {
            if (String.IsNullOrEmpty(directory))
                throw new ArgumentException("No directory was specified to check for");

            // Ensure directory is ended with / to avoid false positives
            if (!directory.EndsWith("/"))
                directory += "/";

            try
            {
                var request = (FtpWebRequest)WebRequest.Create(directory);
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.Credentials = networkCredential;

                using (request.GetResponse())
                {
                    return true;
                }
            }
            catch (WebException)
            {
                return false;
            }
        }
        //Run Job
        public async Task Run(long id)
        {
            BAACDataContext databaseTask = new BAACDataContext(Settings.Default.conectionString);
            JOB job =  databaseTask.JOBs.Where(x => x.ID == id).FirstOrDefault();
            FTP_PROFILE ftpPro = databaseTask.FTP_PROFILEs.Where(x => x.IP == job.IP && x.FTP_USER == job.FTP_USER).FirstOrDefault();
            if (ftpPro == null)
            {
                job.STATUS = "F";
                databaseTask.SubmitChanges();
                return;
            } 
            foreach (JOB_File jobFile in job.JOB_Files)
            {
                try
                {

                    string port = string.IsNullOrEmpty(ftpPro.PORT) ? "21" : ftpPro.PORT;
                    string url = CombineUrlPaths("ftp://" + job.IP + ":" + port, jobFile.TARGETPART);
                    string filename = jobFile.FILENAME.Replace("\"", string.Empty);
                    //Check exist 
                    if (!DirectoryExists(url, new NetworkCredential(ftpPro.FTP_USER, ftpPro.FTP_PASSWORD)))
                    {
                        //Create Directory
                        FtpWebRequest requestMakeDir = (FtpWebRequest)WebRequest.Create(url);
                        requestMakeDir.Method = WebRequestMethods.Ftp.MakeDirectory;
                        requestMakeDir.Credentials = new NetworkCredential(ftpPro.FTP_USER, ftpPro.FTP_PASSWORD);
                        using (var resp = (FtpWebResponse)requestMakeDir.GetResponse())
                        {
                           
                        }
                    }
                    url = CombineUrlPaths(url, filename);
                    FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url);
                    request.Method = WebRequestMethods.Ftp.UploadFile;
                    // This example assumes the FTP site uses anonymous logon.
                    request.Credentials = new NetworkCredential(ftpPro.FTP_USER, ftpPro.FTP_PASSWORD);
                    // Copy the contents of the file to the request stream. 
                    StreamReader sourceStream = new StreamReader(jobFile.FULLPART);
                    byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                    sourceStream.Close();
                    request.ContentLength = fileContents.Length;
                    Stream requestStream = request.GetRequestStream();
                    requestStream.Write(fileContents, 0, fileContents.Length);
                    requestStream.Close();
                    FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                    response.Close();

                    jobFile.STATUS = "E";
                    databaseTask.SubmitChanges();
                }
                catch (Exception ex) {
                    jobFile.STATUS = "F";
                    databaseTask.SubmitChanges();
                }
            }
            job.STATUS = "E";
            databaseTask.SubmitChanges();
        }

        public static string CombineUrlPaths(string basePath, string pagePath)
        {
            if (String.IsNullOrEmpty(basePath) || String.IsNullOrEmpty(pagePath))
            {
                throw new ArgumentNullException((basePath == null) ? "path1" : "path2");
            }
            if (pagePath[0] == char.Parse("/"))
            {
                pagePath = pagePath.Substring(1, pagePath.Length - 1);
            }
            var path = basePath[basePath.Length - 1] != char.Parse("/") ?
                       (String.Format("{0}/{1}", basePath, pagePath)) : (basePath + pagePath);
            return path;
        }
    }
}
